<?php 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 */
class ProductMapper {
    
    /**
     * @param none
     * @return  $resultArray[] Array of Product Objects
     */
    public function getProducts() {
        $dbConn = getDbConnection();

        $queryR = $dbConn->prepare("select pet_name, pet_type, pet_sex, pet_price from pet");
        $queryR->execute();

        $resultArray = array();
        
       while ($row = $queryR->fetch()) {
           $resultArray[] = new Product($row['pet_name'], $row['pet_type'], $row['pet_sex'], $row['pet_price']);
       }

      return $resultArray;
    }
    
    /**
     * Get a single row from the pet table by passing in the primary 
     * key as an integer.
     * 
     * @param int $id
     * @return object Product
     */
   
    public function getProduct($id) {
        $dbConn = getDbConnection();
        $queryR = $dbConn->prepare("select pet_name, pet_type, pet_sex, pet_price from pet where pet_id = ?");
        $queryR->execute(array($id));
       
        
        $row = $queryR->fetch();
        
        return new Product($row['pet_name'], $row['pet_type'],$row['pet_sex'],$row['pet_price']);
    }
    /**
     * Take in a Product class object and insert it into the database 
     * 
     * @param object $product
     * @return boolean $result
     */
    
    public function addNewProduct($product) {
        $dbConn = getDbConnection();
        $queryInsert = $dbConn->prepare("insert into pet (pet_name, pet_type, pet_sex, pet_price) values (?,?,?,?)");
        $result = $queryInsert->execute(array(
            $product->getName(),
            $product->getType(),
            $product->getSex(),
            $product->getPrice()
        ));
        
        return $result;
    }
    /**
     * Gets the last entry in the database by returning the highest primary key number
     * 
     * @param none 
     * @return object Product
     */
     public function getLatestEntry() {
        $dbConn = getDbConnection();
        $queryR = $dbConn->prepare("select pet_name, pet_type, pet_sex, pet_price from pet where pet_id = (select max(pet_id) from pet)");
        $queryR->execute(array());
       
        
        $row = $queryR->fetch();
        
        return new Product($row['pet_name'], $row['pet_type'],$row['pet_sex'],$row['pet_price']);
    }
}

