<?php
session_start();

require('DbConnectLocal.php');
require('Product.php');
require('ProductMapper.php');

if (! isset($_SESSION['logged in'])) {
    header("Location: login.php");
}
 else {
    $message = 'You are now logged in as ' . $_SESSION['username'];
}

$productMapper = new ProductMapper();
$rows = $productMapper->getProducts();
$productMapper1 = new ProductMapper();
$single = $productMapper1->getLatestEntry();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Pet Store Products</title>
        <link rel='stylesheet' type="text/css" href="petStore.css">
    </head>
    <body>
        <div id='container'>
            
            <?php
            if (isset($message)) {
                echo '<h4 style="color: red;">' . $message . '</h4>';
            }
            ?>
            
            <header id="top">
                <h1 class="heading">Tallahassee Pet Store</h1>
            </header>
            <nav id="navi">
                <h3 class="heading">Inventory Page</h3>
            </nav> 
            <section id="main" style=" float:left;">
                <h3 class="heading">Full Inventory</h3>
                <table class="pTable">
                    <thead>
                        <tr>
                            <th>Pet Name</th>
                            <th>Pet Type</th>
                            <th>Pet Sex</th>
                            <th>Pet Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($rows as $row) {
                            echo "<tr>";
                            echo "<td>{$row->getName()}</td>";
                            echo "<td>{$row->getType()}</td>";
                            echo "<td>{$row->getSex()}</td>";
                            echo "<td>{$row->getPrice()}</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </section>
            <aside id='side' style="float: right;">
                <h3 class="heading">Last Entry</h3>
                <table class="pTable">
                    <thead>
                        <tr>
                            <th>Pet Name</th>
                            <th>Pet Type</th>
                            <th>Pet Sex</th>
                            <th>Pet Price</th>
                        </tr>
                    </thead>
                     <tbody>
                        <?php
                            echo "<tr>";
                            echo "<td>{$single->getName()}</td>";
                            echo "<td>{$single->getType()}</td>";
                            echo "<td>{$single->getSex()}</td>";
                            echo "<td>{$single->getPrice()}</td>";
                            echo "</tr>";
                        ?>
                    </tbody>
                </table>
            </aside>
            <footer id="foot">
                <h4 class="heading">Tallahassee Pet Stores is solely 
                    owned by Nathaniel Worrell
                <br>
                <br>
                1234 Park Ave Tallahassee, Fl 32301
                <br>
                (850)555-5555
                </h4>
            </footer>
        </div>    
    </body>
</html>
