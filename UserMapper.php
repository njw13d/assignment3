<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserMapper
 *
 * @author Nate
 */
class UserMapper {
    
    /**
     * Get the user information from a database 
     * 
     * @param type $usr_name
     * @return \User
     * @throws Exception
     */
    public function getUser($usr_name) {
        $dbConn = getDbConnection();
        $query = $dbConn->prepare("select usr_name, usr_pass from user where usr_name = ?");
        $query->execute(array($usr_name));
        
        $row = $query->fetch();
        
        if ($row !== FALSE) {
            return new User($row['usr_name'], $row['usr_pass']);
        }
        else {
            throw new Exception("Could not find username:" . $usr_name);
        }
    }
}
