<?php

session_start();

require('DbConnectLocal.php');
require('Product.php');
require('ProductMapper.php');
require('vendor/autoload.php');

use SimpleValidator\Validator;
use SimpleValidator\Validators;

if (! isset($_SESSION['logged in'])) {
    header("Location: login.php");
}
 else {
    $message = 'You are now logged in as ' . $_SESSION['username'];
}

// Error messages 

$pNameRequiredErr = 'Pet Name is Required';
$pTypeRequiredErr = 'Pet Type is Required';
$pSexRequiredErr = 'Pet Sex is Required';
$pPriceRequiredErr = 'Pet Price is Required';
$pNameAlphaNumErr = 'Pet Name must be an alpha numeric entry';
$pTypeAlphaNumErr = 'Pet Type must be an alpha numeric entry';
$pSexAlphaNumErr = 'Pet Sex must be an alpha numeric entry m or f';
$pSexMaxLengthErr = 'Pet Sex may only be one letter either m or f';
$pPriceNumericErr = 'Pet Price must be a numeric value';


if (count($_POST) > 0){
    
    $valCheck = new Validator($_POST,array(
        new Validators\Required('pName', $pNameRequiredErr),
        new Validators\Required('pType', $pTypeRequiredErr),
        new Validators\Required('pSex', $pSexRequiredErr),
        new Validators\Required('pPrice', $pPriceRequiredErr),
        new Validators\AlphaNumeric('pName', $pNameAlphaNumErr),
        new Validators\AlphaNumeric('pType', $pTypeAlphaNumErr ),
        new Validators\AlphaNumeric('pSex', $pSexAlphaNumErr),
        new Validators\MaxLength('pSex', $pSexMaxLengthErr, 1),
        new Validators\Numeric('pPrice', $pPriceNumericErr)    
    ));
    
    $valCheck->execute();

    foreach ($valCheck->getErrors(each($_POST)) as $error) {
    echo "<p>" . $error[0] . "</p>";
    }



        $productEntry = new Product($_POST['pName'], $_POST['pType'], $_POST['pSex'], $_POST['pPrice']);
        $mapper = new ProductMapper();
        $result = $mapper->addNewProduct($productEntry);

        if ($result == TRUE) {
            header("Location: index.php");    
        }
        else {
            $msg = 'Information was not entered';
        }
    }
else {
    header("Location: addProductPage.php");
}

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Submit</title>
         <link rel='stylesheet' type="text/css" href="petStore.css">
    </head>
    <body>
        <div id="container">
            
            <?php
            if (isset($message)) {
                echo '<h4 style="color: red;">' . $message . '</h4>';
            }
            ?>
            
            <nav id="navi" style="padding: 50px;">
                <h3 class="heading">Submission Error</h3>
            </nav> 
            <br>
            <div id="main" style="min-height: 300px; min-width: 100%;">
                <br>
                <?php
                if (isset($msg)){
                echo '<h2><span style="color: red;">' . $msg . '</span></h2><br>
                     <a style="text-align: center;" href="addProductPage.php.php">Try Again</a>';
                }
                ?>
            </div>    
            <footer id="foot">
                <h4 class="heading">Tallahassee Pet Stores is solely 
                    owned by Nathaniel Worrell
                <br>
                <br>
                1234 Park Ave Tallahassee, Fl 32301
                <br>
                (850)555-5555
                </h4>
            </footer>
        </div>
    </body>
</html>
