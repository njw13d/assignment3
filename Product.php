<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of product
 *
 * @author Nate
 */
class Product {
    protected $pet_name;
    protected $pet_type;
    protected $pet_sex;
    protected $pet_price;


    //-----------------------------------------------------
   /**
    * Class Product Constructor
    * 
    * @param type $pet_name
    * @param type $pet_type
    * @param type $pet_sex
    * @param type $pet_price
    */
    public function __construct($pet_name, $pet_type, $pet_sex, $pet_price){
        $this->setName($pet_name);
        $this->setType($pet_type);
        $this->setSex($pet_sex);
        $this->setPrice($pet_price);
    }
  //--------------------------------------------------------
    
    /**
     * Setter for pet_name
     * 
     * @param type $pet_name
     */
    public function setName($pet_name) {
        $this->pet_name = $pet_name;
    }
    
    /**
     * Setter for pet_type
     * 
     * @param type $pet_type
     */
    public function setType($pet_type) {
        $this->pet_type = $pet_type;
    }
    
    /**
     * Setter for pet_sex
     * 
     * @param type $pet_sex
     */
    public function setSex($pet_sex) {
        $this->pet_sex = $pet_sex;
    }
    
    /**
     * Setter for pet_price
     * 
     * @param type $pet_price
     */
    public function setPrice($pet_price) {
        $this->pet_price = $pet_price;
    }
    
    /**
     * Getter for pet_name
     * 
     * @return $pet_name
     */
    public function getName(){
        return $this->pet_name;
    }
    
    /**
     * Getter for pet_type
     * 
     * @return $pet_type
     */
    public function getType() {
        return $this->pet_type;
    }
    
    /**
     * Getter for pet_sex
     * 
     * @return $pet_sex
     */
    public function getSex() {
        return $this->pet_sex;
    }
    
    /**
     * Getter for pet_price
     * 
     * @return $pet_price
     */
    public function getPrice() {
        return $this->pet_price;
    }
    
}