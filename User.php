<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Nate
 */
class User {
    
    protected $usr_name;
    protected $usr_pass;
    
    /**
     * User class constructor
     * 
     * @param type $usr_name
     * @param type $usr_pass
     */
    public function __construct($usr_name, $usr_pass) {
        $this->usr_name = $usr_name;
        $this->usr_pass = $usr_pass;
    }
    
    /**
     * Getter for usr_name
     * 
     * @return $usr_name
     */
    public function getName() {
        return $this->usr_name;
    }
    
    /**
     * Getter for usr_pass
     * 
     * @return usr_pass
     */
    public function getPass() {
        return $this->usr_pass;
    }
}