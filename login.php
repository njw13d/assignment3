<?php

session_start();

require ('DbConnectLocal.php');
require ('User.php');
require ('UserMapper.php');


$userMapper = new UserMapper();

// Process form if submitted 

if (count($_POST) > 0) {
    $usr_name = $_POST['uName'];
    $usr_pass = $_POST['uPass'];
    
    
    try {
        $userObject = $userMapper->getUser($usr_name);
        
        // Check if usr_name exists in database 
        if ($usr_pass == $userObject->getPass()) {
            $_SESSION['logged in'] = TRUE;
            $_SESSION['username'] = $usr_name;
            header("Location: addProductPage.php");
        }
        else {
            $msg = "Bad Password";
        }
    } catch (Exception $ex) {
        $msg = "Invalid Username";
    }
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link rel='stylesheet' type="text/css" href="petStore.css">
    </head>
    <body>
        <div id="container">
             <nav id="navi">
                <h3 class="heading">Login Page</h3>
            </nav> 
            <h1 style="text-align: center;">Please Log In to access the PetStore Database.</h1>
            <div id="main" style="margin-left: auto; margin-right: auto; padding: 20px;">
                <?php if (isset($msg)) {echo $msg;} ?>
                <form method="post">
                    <table class="pTable" style="margin-top: 10px;">
                        <tr>
                            <td>
                                <label for="uName">Username:</label>
                            </td>
                            <td>
                                <input type="text" name="uName" id="uName">
                            </td>
                        </tr>    
                        <tr>    
                            <td>
                                <label for="uPass">Password:</label>
                            </td>
                            <td>
                                <input type="password" name="uPass" id="uPass">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Submit">
                            </td>    
                        </tr>
                    </table> 
                </form>    
            </div>
            <br>
            <br>
            <br>
            <footer id="foot">
                <h4 class="heading">Tallahassee Pet Stores is solely 
                    owned by Nathaniel Worrell
                <br>
                <br>
                1234 Park Ave Tallahassee, Fl 32301
                <br>
                (850)555-5555
                </h4>
            </footer>
        </div>
    </body>
</html>
