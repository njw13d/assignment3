<?php
session_start();

require ('DbConnectLocal.php');
require ('User.php');
require ('UserMapper.php');
require('vendor/autoload.php');

if (! isset($_SESSION['logged in'])) {
    header("Location: login.php");
}
 else {
    $message = 'You are now logged in as ' . $_SESSION['username'];
}

?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>AddProductPage</title>
        <link rel='stylesheet' type="text/css" href="petStore.css">
    </head>
    <body>
        <div id="container">
            
            <?php
            if (isset($message)) {
                echo '<h4 style="color: red;">' . $message . '</h4>';
            }
            ?>
            
         <header id="top">
                <h1 class="heading">Tallahassee Pet Store</h1>
            </header>
            <br>
            <br>
            <br>
            <nav id="navi">
                <h3 class="heading">New Product Page</h3>
            </nav> 
            <br>
            <br>
            <br>
            <section id="side" style="margin-left: auto; margin-right: auto; padding: 20px;">
                <form method="post" action="submit.php">
                    <table class="pTable" style="margin-top: 10px;">
                        <tr>
                            <td>
                                <label for="pName">Pet Name:</label>
                            </td>
                            <td>
                                <input type="text" name="pName" id="pName">
                            </td>
                        </tr>    
                        <tr>    
                            <td>
                                <label for="pType">Pet Type:</label>
                            </td>
                            <td>
                                <input type="text" name="pType" id="pType">
                            </td>
                        </tr>    
                        <tr>
                            <td>
                                <label for="pSex">Pet Sex:</label>
                            </td>
                            <td>
                                <input type="text" name="pSex" id="pSex">
                            </td>
                        </tr>    
                        <tr>  
                            <td>
                                <label for="pPrice">Pet Price:</label>
                            </td>
                            <td>
                                <input type="text" name="pPrice" id="pPrice">
                            </td>
                        </tr>    
                        <tr>
                            <td>
                                <input type="submit" name="submit" value="Submit">
                            </td>    
                        </tr>    
                    </table>
                </form>
            </section> 
            <br>
            <br>
            <footer id="foot" style="padding-top: 20px;">
                <h4 class="heading">Tallahassee Pet Stores is solely 
                    owned by Nathaniel Worrell
                <br>
                <br>
                1234 Park Ave Tallahassee, Fl 32301
                <br>
                (850)555-5555
                </h4>
            </footer>
        </div>
    </body>
</html>
